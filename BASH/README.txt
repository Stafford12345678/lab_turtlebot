**** START LAB ****
***
**
*

*
**
***
**** OVERALL SOLUTION ****

-Have one turtlebot make a map using teleop_keyboard, then save, add, commit and send that map to a bitbucket repository for other turtlebots to use, navigate and update as needed. 

**** OVER ALL SOLUTION ****
***
**
*


*
**
***
**** HOW TO SET UP THE LAB ****

- Open a terminal inside the BASH directory

- Then you can type in the command as shown: ./<name of file>

 - On the remote PC, run RemoteComputer.sh
	-brings up the keyboard teleop

 - Save the generated or updated map and push it to the online repository  using the bashsave.sh file

 - Run Turtlebot2.sh
 	- this will open up rviz with the map data


**** HOW TO SET UP THE LAB ****
***
**
*

*
**
***
**** END LAB ****
