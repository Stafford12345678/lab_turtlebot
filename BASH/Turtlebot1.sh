#!/bin/bash

clear

echo "Turtlebot 1 Startup..."

#starts up a new terminal with roscore command
gnome-terminal -e "roscore"

#sleeps for 4 seconds
sleep 4s

clear 
echo "Launching Turtlebot 1..."

#starts up a new terminal with command:
gnome-terminal -e "roslaunch turtlebot_bringup minimal.launch"

#same
sleep 4s
# ---
gnome-terminal -e "roslaunch turtlebot_navigation gmapping_demo.launch"

#same
sleep 4s
# ---
gnome-terminal -e "roslaunch turtlebot_rviz_launchers view_navigation.launch --screen"






