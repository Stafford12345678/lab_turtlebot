#!/bin/bash

clear

echo "Turtlebot 2 Startup..."

sleep 2s

git pull # receiving any updates to the costmap 

sleep 5s

clear

echo "Launching Turtlebot 2"

gnome-terminal -e "roslaunch turtlebot_bringup minimal.launch"

sleep 4s
# launching a map file recently updated
gnome-terminal -e "roslaunch turtlebot_navigation amcl_demo.launch map_file:=/home/turtlebot/lab_turtlebot/map.yaml"


sleep 4s
# launching the rviz navigation viewer with the map from turtlebot 1
gnome-terminal -e "roslaunch turtlebot_rviz_launchers view_navigation.launch --screen"


